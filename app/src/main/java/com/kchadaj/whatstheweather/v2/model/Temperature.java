package com.kchadaj.whatstheweather.v2.model;

import com.google.gson.annotations.SerializedName;

public class Temperature {

    private static final String DEGREE_SYMBOL = "°";

    @SerializedName("temp_max")
    private Double temp = null;

    @SerializedName("temp_min")
    private Double tempMin = null;

    public String printTemp() {
        return Math.round(temp) + DEGREE_SYMBOL;
    }

    public String printLowTemp() {
        return Math.round(tempMin) + DEGREE_SYMBOL;
    }
}
