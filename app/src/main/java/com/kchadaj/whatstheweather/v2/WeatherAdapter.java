package com.kchadaj.whatstheweather.v2;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kchadaj.whatstheweather.v2.model.ForecastData;

import java.util.ArrayList;

class WeatherAdapter extends RecyclerView.Adapter<WeatherReportViewHolder> {

    private ArrayList<ForecastData> mForecastData;

    WeatherAdapter(ArrayList<ForecastData> forecastData) {
        mForecastData = forecastData;
    }

    @Override
    public WeatherReportViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View card = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_weather, parent, false);
        return new WeatherReportViewHolder(card);
    }

    @Override
    public void onBindViewHolder(WeatherReportViewHolder holder, int position) {
        holder.updateCardUI(mForecastData.get(position));
    }

    @Override
    public int getItemCount() {
        return mForecastData.size();
    }
}
