package com.kchadaj.whatstheweather.v2.model;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

import java.util.ArrayList;

public class Forecast {

    private static final String HTTP_OK = "200";

    @SerializedName("cod")
    private String statusCode = null;

    @SerializedName("list")
    private ArrayList<ForecastData> forecastData = null;

    private City city = null;

    public ArrayList<ForecastData> getForecastData() {
        return forecastData;
    }

    public City getCity() {
        return city;
    }

    public boolean isValid() {
        return HTTP_OK.equals(statusCode);
    }

    public static Forecast fromJson(JSONObject response) {
        return new Gson().fromJson(response.toString(), Forecast.class);
    }
}
