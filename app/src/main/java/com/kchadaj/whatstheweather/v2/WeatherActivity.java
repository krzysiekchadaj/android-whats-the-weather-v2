package com.kchadaj.whatstheweather.v2;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.kchadaj.whatstheweather.v2.model.Forecast;
import com.kchadaj.whatstheweather.v2.model.ForecastData;

import org.json.JSONObject;

import java.util.ArrayList;

public class WeatherActivity extends AppCompatActivity {

    private final static String TAG = "WeatherActivity";

    private final static String URL_BASE = "http://api.openweathermap.org/data/2.5/forecast?";
    private final static String URL_API_KEY = "APPID=--->to be replaced with this API key<---";
    private final static String URL_UNITS = "&units=metric";
    private final static String URL_COORDINATE_LAT = "&lat=";
    private final static String URL_COORDINATE_LON = "&lon=";

    private final int PERMISSION_REQUEST_ACCESS_FINE_LOCATION = 1001;

    private TextView weatherDate;
    private TextView weatherTemp;
    private TextView weatherLowTemp;
    private TextView weatherCity;
    private TextView weatherDescription;
    private ImageView weatherIcon;
    private RecyclerView weatherReports;

    WeatherAdapter weatherAdapter;
    ArrayList<ForecastData> mForecastData;

    FusedLocationProviderClient mFusedLocationProviderClient;
    LocationCallback mLocationCallback;
    RequestQueue mRequestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        weatherDate = findViewById(R.id.weather_date);
        weatherTemp = findViewById(R.id.weather_temp);
        weatherLowTemp = findViewById(R.id.weather_low_temp);
        weatherCity = findViewById(R.id.weather_city);
        weatherDescription = findViewById(R.id.weather_description);
        weatherIcon = findViewById(R.id.weather_icon);
        weatherReports = findViewById(R.id.content_weather_reports);

        mFusedLocationProviderClient = new FusedLocationProviderClient(this);
        mRequestQueue = Volley.newRequestQueue(this.getApplicationContext());

        prepareWeatherReportsAdapter();

        requestLocationPermission();
    }

    private void prepareWeatherReportsAdapter() {
        mForecastData = new ArrayList<>();
        weatherAdapter = new WeatherAdapter(mForecastData);
        weatherReports.setAdapter(weatherAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        weatherReports.setLayoutManager(layoutManager);
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (mLocationCallback != null) {
            mFusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
        }

        mRequestQueue.stop();
    }

    private void requestLocationPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                PERMISSION_REQUEST_ACCESS_FINE_LOCATION);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case PERMISSION_REQUEST_ACCESS_FINE_LOCATION:
                if (isLocationPermitted(grantResults)) {
                    requestLocation();
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.toast_gps_required), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private boolean isLocationPermitted(int[] grantResults) {
        if (grantResults.length > 0) {
            for (int result : grantResults) {
                if (result != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    private void requestLocation() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            mLocationCallback = new LocationCallback() {
                @Override
                public void onLocationAvailability(LocationAvailability locationAvailability) {
                    super.onLocationAvailability(locationAvailability);

                    if (!locationAvailability.isLocationAvailable()) {
                        Toast.makeText(getApplicationContext(), getString(R.string.toast_gps_off), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onLocationResult(LocationResult locationResult) {
                    super.onLocationResult(locationResult);

                    downloadWeatherData(locationResult.getLastLocation());
                }
            };

            mFusedLocationProviderClient.requestLocationUpdates(
                    new LocationRequest().setPriority(LocationRequest.PRIORITY_LOW_POWER),
                    mLocationCallback,
                    getMainLooper());
        }
    }

    private void downloadWeatherData(Location location) {
        final String url = URL_BASE + URL_API_KEY + URL_UNITS
                + URL_COORDINATE_LAT + location.getLatitude()
                + URL_COORDINATE_LON + location.getLongitude();

        mRequestQueue.add(new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Forecast forecast = Forecast.fromJson(response);
                        if (forecast.isValid()) {
                            updateUI(forecast);
                            updateCardsUI(forecast);
                        } else {
                            Toast.makeText(getApplicationContext(), getString(R.string.toast_response_not_ok_error), Toast.LENGTH_SHORT).show();
                            Log.d(TAG, getString(R.string.toast_response_not_ok_error));
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), getString(R.string.toast_response_error), Toast.LENGTH_SHORT).show();
                        Log.d(TAG, getString(R.string.toast_response_error), error);
                    }
                }
        ));
    }

    private void updateUI(Forecast forecast) {
        weatherCity.setText(forecast.getCity().toString());

        ForecastData forecastData = forecast.getForecastData().get(0);
        weatherDate.setText(forecastData.printDate());
        weatherTemp.setText(forecastData.getTemperature().printTemp());
        weatherLowTemp.setText(forecastData.getTemperature().printLowTemp());
        weatherDescription.setText(forecastData.getWeatherDescription().printDescription());
        weatherIcon.setImageDrawable(getResources().getDrawable(forecastData.getWeatherDescription().getIconDrawable()));
    }

    private void updateCardsUI(Forecast forecast) {
        mForecastData.clear();
        mForecastData.addAll(forecast.getForecastData());
        weatherAdapter.notifyDataSetChanged();
    }
}
