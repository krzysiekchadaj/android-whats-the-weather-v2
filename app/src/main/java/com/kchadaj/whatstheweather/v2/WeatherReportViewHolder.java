package com.kchadaj.whatstheweather.v2;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.kchadaj.whatstheweather.v2.model.ForecastData;

class WeatherReportViewHolder extends RecyclerView.ViewHolder {

    private ImageView cardWeatherIcon;
    private TextView cardWeatherDate;
    private TextView cardWeatherDescription;
    private TextView cardWeatherTemp;
    private TextView cardWeatherLowTemp;

    WeatherReportViewHolder(View itemView) {
        super(itemView);

        cardWeatherIcon = itemView.findViewById(R.id.card_weather_icon);
        cardWeatherDate = itemView.findViewById(R.id.card_weather_date);
        cardWeatherDescription = itemView.findViewById(R.id.card_weather_description);
        cardWeatherTemp = itemView.findViewById(R.id.card_weather_temp);
        cardWeatherLowTemp = itemView.findViewById(R.id.card_weather_low_temp);
    }

    void updateCardUI(ForecastData forecastData) {
        cardWeatherDate.setText(forecastData.printDate());
        cardWeatherTemp.setText(forecastData.getTemperature().printTemp());
        cardWeatherLowTemp.setText(forecastData.getTemperature().printLowTemp());
        cardWeatherDescription.setText(forecastData.getWeatherDescription().printDescription());
        cardWeatherIcon.setImageDrawable(itemView.getResources().getDrawable(forecastData.getWeatherDescription().getIconDrawable()));
    }
}
