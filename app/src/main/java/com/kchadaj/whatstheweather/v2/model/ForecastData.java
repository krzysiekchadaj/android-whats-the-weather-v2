package com.kchadaj.whatstheweather.v2.model;

import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class ForecastData {
    private static final String TAG = "ForecastData";

    @SerializedName("main")
    private Temperature temperature = null;

    @SerializedName("weather")
    private ArrayList<WeatherDescription> weatherDescription = null;

    @SerializedName("dt_txt")
    private String rawDate = null;

    public Temperature getTemperature() {
        return temperature;
    }

    public WeatherDescription getWeatherDescription() {
        return weatherDescription.get(0);
    }

    private String convertRawDate(String rawDate) {
        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(rawDate);
            return new SimpleDateFormat("E d, HH:mm", Locale.ENGLISH).format(date);
        } catch (ParseException e) {
            Log.d(TAG, "Unable to parse date received from the server.", e);
        }
        return "";
    }

    public String printDate() {
        return convertRawDate(rawDate);
    }
}
