package com.kchadaj.whatstheweather.v2.model;

import com.kchadaj.whatstheweather.v2.R;

import static com.kchadaj.whatstheweather.v2.model.WeatherConditions.*;

public class WeatherDescription {

    private Integer id = null;

    public int getId() {
        return id;
    }

    public String printDescription() {
        return WeatherConditions.DESCRIPTION.get(id);
    }

    public int getIconDrawable() {
        int drawable = 0;
        switch (id) {
            case THUNDERSTORM_WITH_LIGHT_RAIN:
            case THUNDERSTORM_WITH_RAIN:
            case THUNDERSTORM_WITH_HEAVY_RAIN:
            case LIGHT_THUNDERSTORM:
            case THUNDERSTORM:
            case HEAVY_THUNDERSTORM:
            case RAGGED_THUNDERSTORM:
            case THUNDERSTORM_WITH_LIGHT_DRIZZLE:
            case THUNDERSTORM_WITH_DRIZZLE:
            case THUNDERSTORM_WITH_HEAVY_DRIZZLE:
                drawable = R.drawable.thunder;
                break;

            case LIGHT_INTENSITY_DRIZZLE:
            case DRIZZLE:
            case HEAVY_INTENSITY_DRIZZLE:
            case LIGHT_INTENSITY_DRIZZLE_RAIN:
            case DRIZZLE_RAIN:
            case HEAVY_INTENSITY_DRIZZLE_RAIN:
            case SHOWER_RAIN_AND_DRIZZLE:
            case HEAVY_SHOWER_RAIN_AND_DRIZZLE:
            case SHOWER_DRIZZLE:
            case LIGHT_RAIN:
            case MODERATE_RAIN:
            case HEAVY_INTENSITY_RAIN:
            case VERY_HEAVY_RAIN:
            case EXTREME_RAIN:
            case FREEZING_RAIN:
            case LIGHT_INTENSITY_SHOWER_RAIN:
            case SHOWER_RAIN:
            case HEAVY_INTENSITY_SHOWER_RAIN:
            case RAGGED_SHOWER_RAIN:
                drawable = R.drawable.rainy;
                break;

            case LIGHT_SNOW:
            case SNOW:
            case HEAVY_SNOW:
            case SLEET:
            case SHOWER_SLEET:
            case LIGHT_RAIN_AND_SNOW:
            case RAIN_AND_SNOW:
            case LIGHT_SHOWER_SNOW:
            case SHOWER_SNOW:
            case HEAVY_SHOWER_SNOW:
            case HAIL:
                drawable = R.drawable.snow;
                break;

            case CLEAR_SKY:
            case HOT:
            case COLD:
            case WINDY:
            case CALM:
                drawable = R.drawable.sunny;
                break;

            case FEW_CLOUDS:
            case SCATTERED_CLOUDS:
                drawable = R.drawable.partially_cloudy;
                break;

            case BROKEN_CLOUDS:
            case OVERCAST_CLOUDS:
                drawable = R.drawable.cloudy;
                break;

            case MIST:
            case SMOKE:
            case HAZE:
            case DUSTY_WHIRLS:
            case FOG:
            case SAND:
            case DUST:
            case VOLCANIC_ASH:
            case SQUALLS:
            case TORNADO:
            case TORNADO2:
            case TROPICAL_STORM:
            case HURRICANE:
            case LIGHT_BREEZE:
            case GENTLE_BREEZE:
            case MODERATE_BREEZE:
            case FRESH_BREEZE:
            case STRONG_BREEZE:
            case HIGH_WIND_NEAR_GALE:
            case GALE:
            case SEVERE_GALE:
            case STORM:
            case VIOLENT_STORM:
            case HURRICANE2:
            default:
                break;
        }
        return drawable;
    }
}
