# [Android] What's the Weather? v2
An Android application for the weather forecast written in Java. Utilizes [OpenWeatherMap API](http://openweathermap.org/api).

![What's the Weather? v2 screen](img/screen.jpg)

## Libraries and frameworks:
|               | **Location**            | **Web request**   | **Response parser** |
|:-------------:|:-----------------------:|:-----------------:|:-------------------:|
| **Version 1** | Location Manager        | HttpURLConnection | org.json            |
| **Version 2** | Fused Location Provider | Volley            | Gson                |
| **Version 3** | Fused Location Provider | Retrofit2         | (Gson)              |

## Handwritten lines of code:
|               | **Java** | **XML** |
|:-------------:|:--------:|:-------:|
| **Version 1** | 653        | 399   |
| **Version 2** | 672        | 283   |
| **Version 3** | 695        | 283   |

## Disclaimer
You can use the software for educational purposes only. The information
regarding the origin of the software shall be preserved.

The software is provided "as is", and without warranty of any kind. In 
no event shall the author be liable for any claim, tort, damage, or any
other liability.

By using the program, you agree to the above terms and conditions.
